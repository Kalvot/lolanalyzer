<?php

use Illuminate\Database\Seeder;

class ChampionsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $config = [
            'api_key' => '4f491c30-db3e-43a0-b110-59a4162ae6aa',
            'region' => 'eune',
            'summoner_v' => 'v1.4',
            'stats_v' => 'v1.3',
            'lol_static_data_v' => 'v1.2',
            'game_v' => '6.11.1'
        ];
        $client = new GuzzleHttp\Client([
            'base_uri' => 'https://' . $config['region'] . '.api.pvp.net/api/lol/' . $config['region'] . '/',
            'timeout' => 15.0,
            'verify' => false
        ]);
        $url = 'https://global.api.pvp.net/api/lol/static-data/eune/' .
                $config['lol_static_data_v'] . '/champion?champData=all&api_key=' . $config['api_key'];
        $champions = json_decode($client->get($url)->getBody());
        $keys = $champions->keys;
        $data = $champions->data;
        foreach ($keys as $id => $name) {
            DB::table('champions')->insert([
                'id' => $id,
                'name' => $name,
                'data' => serialize($data->$name),
            ]);
        }
    }

}

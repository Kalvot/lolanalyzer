@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="row">
                        <a href="{{url('/'.$player->data->name).'/renew'}}" class="btn-info">
                            Renew
                        </a>
                    </div>
                    <div class="row player">
                        {{ $player->data->name }} <br>
                        ID: {{$player->id}}<br>
                    </div>

                    <div class="col-sm-3">
                        @foreach ($stats as $stat)

                        <div class="row champion">
                            <div class="col-sm-3">
                                <img src="{{$stat->avatar_url}}" style="height: 40px; width: auto;">
                            </div>
                            <div class="col-sm-9">
                                {{$stat->name}} <br>

                                Winratio: {{ $stat->winratio }} %  {{$stat->wins}}/{{$stat->lost}}
                            </div>

                        </div>

                        @endforeach
                    </div>
                    <div class="col-sm-9">
                        @foreach ($matches as $match)
                        @if($match->playerStats->winner)
                        <div class="row match win">
                            @else
                            <div class="row match lose">
                                @endif
                                <div class="col-sm-6">
                                    {{ $match->queue }}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            KDA: {{$match->playerStats->kills}}/{{$match->playerStats->deaths}}/{{$match->playerStats->assists}} <br>
                                        </div>
                                        <div class="col-sm-6">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        @for($i=0; $i<5; $i++ )
                                        <div class="summoner">
                                            <img src="{{$match->data->participants[$i]->champion }}" style="height: 20px; width: auto;">
                                            <a href="{{url('/'.$match->players[$i]->player->summonerName)}}">
                                                @if( $match->players[$i]->player->summonerName == $player->data->name )
                                                <strong>{{$player->data->name}}</strong><br>
                                                @else
                                                {{$match->players[$i]->player->summonerName }} <br>
                                                @endif
                                            </a>
                                        </div>

                                        @endfor
                                    </div>
                                    <div class="col-sm-6">
                                        @for($i=5; $i<10; $i++ )
                                        <div class="summoner">
                                            <img src="{{$match->data->participants[$i]->champion }}" style="height: 20px; width: auto;">
                                            <a href="{{url('/'.$match->players[$i]->player->summonerName)}}">
                                                @if( $match->players[$i]->player->summonerName == $player->data->name )
                                                <strong>{{$player->data->name}}</strong><br>
                                                @else
                                                {{$match->players[$i]->player->summonerName }} <br>
                                                @endif
                                            </a>


                                        </div>
                                        @endfor
                                    </div>
                                </div>

                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

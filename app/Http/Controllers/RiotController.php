<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\PrepareData;
use App\Http\Requests;

class RiotController extends Controller {

    protected $PrepareData;
    public function __construct() {
    }

    public function index(Request $request, $name) {
        $this->PrepareData = new PrepareData($name);
        $stats = $this->PrepareData->prepareStats();
        $matches = $this->PrepareData->prepareMatches();
        $player = $this->PrepareData->getPlayer();
        
        return view('home', [
            'player' => $player,
            'stats' => (object) array_slice($stats, 0, 5),
            'matches' => $matches,
        ]);
    }
    
    public function renew(Request $request, $name){
        $this->PrepareData = new PrepareData($name);
        $matches = $this->PrepareData->prepareMatches(true);
        return redirect('/'.$name);
    }
}

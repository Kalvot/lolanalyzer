<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp;
use App\Http\Requests;

class GitController extends Controller
{
    protected $client;
    
    public function __construct() {
        $this->client = new GuzzleHttp\Client(['base_uri' => 'https://api.github.com/','timeout'  => 2.0, 'verify' => false] );
    }
    public function index(){
        $user = 'KJastrzebski1';
        $about = json_decode($this->client->get('/users/'.$user)->getBody());
        $repos = json_decode($this->client->get('/users/'.$user.'/repos')->getBody());
        return view('home', ['repos' => $repos, 'user' => $about]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = [
        'id',
        'players',
        'data',
    ];
    
    protected $casts = [
        'id' => 'integer',
    ];
}

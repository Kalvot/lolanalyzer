<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class GetStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:getstats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It returns stats about the site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table = DB::table('matches')->get();
        $size = 0;
        foreach ($table as $record){
            $size += strlen(serialize($record));
        }
        $summoners = DB::table('players')->count('*');
        $matches = DB::table('matches')->count('*');
        $this->info("Size of matches table: ". round($size/1024/1024, 2) ." MiB");
        $this->info("Summoners: ". $summoners);
        $this->info("Matches: ".$matches);
    }
}

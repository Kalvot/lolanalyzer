<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class RemoveMatches extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:remove {arg=month}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes matches older than given argument. Choose month, week or day. '
            . 'You can put number of days instead.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $arg = $this->argument('arg');
        $modifier;
        if (is_string($arg)) {
            switch ($arg) {
                case 'month':
                    $modifier = (30 * 24 * 60 * 60);
                    break;

                case 'week':
                    $modifier = (7 * 24 * 60 * 60);
                    break;

                case 'day':
                    $modifier = (1 * 24 * 60 * 60);
                    break;

                default:
                    $modifier = (int)$arg *24*60*60;
                    break;
            }
        }
        $time = time() - $modifier;
        $date = date('Y-m-d H:i:s', $time);
        $affected = DB::table('matches')->where('created_at', '<', $date)->delete();
        $this->info("Records deleted: " . $affected);
        $this->info("Affected matches before: " . $date);
    }

}

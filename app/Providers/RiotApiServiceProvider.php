<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\RiotApi;

class RiotApiServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('\App\Helpers\RiotApi', function(){
            return new RiotApi;
        });
    }
    
    public function provides() {
        return ['\App\Helpers\RiotApi'];
    }
}

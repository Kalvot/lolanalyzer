<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
        'id',
        'name',
        'matches',
        'stats',
        'data',
    ];
    protected $casts = [
        'id' => 'integer',
    ];
}

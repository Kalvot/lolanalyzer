<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use App\Helpers\RiotApi;

class PrepareData {
    
    protected $RiotApi;
    protected $player;
    public function __construct($name) {
        $this->RiotApi = new RiotApi;
        
        $this->player = $this->RiotApi->getSummonerByName($name);
        
    }
    public function getPlayer(){
        return $this->player;
    }
    public function prepareMatches($new=false, $begin = 0, $end = 10){
        $matchList = $this->RiotApi->getMatchList($this->player->id, $begin, $end, $new);
        
        $matches = [];
        foreach ($matchList as $match) {
            $game = $this->RiotApi->getMatch($match->matchId);
            $que = $this->getQueueName($game->data->queueType);
            $game->queue = $que;
            foreach ($game->data->participants as $summoner){
                
                $summoner->champion = $this->RiotApi->getChampionAvatar($summoner->championId);
            }
            foreach ($game->data->participantIdentities as $summoner){
                if($summoner->player->summonerId == $this->player->id){
                    $game->playerStats = $game->data->participants[$summoner->participantId-1]->stats;
                }
            }
            $matches[] = $game;
        }
        
        return $matches;
    }
    
    protected function getQueueName($que){
        switch ($que){
            case 'TEAM_BUILDER_DRAFT_RANKED_5x5':
                $que = 'Ranked';
                break;
            default:
                break;
        }
        return $que;
    }


    public function prepareStats(){
        $stats = $this->RiotApi->getRankedStats($this->player->id)->champions;
        
        $finalStats = [];
        foreach ($stats as $stat) {
            if ($stat->id) {
                $id = $stat->id;
                $finalStats[] = (object) [
                            "id" => $id,
                            "name" => $this->RiotApi->getChampionData($id)->name,
                            "avatar_url" => $this->RiotApi->getChampionAvatar($stat->id),
                            "gamesPlayed" => $stat->stats->totalSessionsPlayed,
                            "wins" => $stat->stats->totalSessionsWon,
                            "lost" => $stat->stats->totalSessionsLost,
                            "winratio" => round($stat->stats->totalSessionsWon / $stat->stats->totalSessionsPlayed, 2) * 100,
                ];
            }
        }
        usort($finalStats, function($a, $b) {
            if ($a->gamesPlayed == $b->gamesPlayed) {
                return 0;
            }
            return ($a->gamesPlayed > $b->gamesPlayed) ? -1 : 1;
        });
        return $finalStats;
    }
}

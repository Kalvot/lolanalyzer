<?php

namespace App\Helpers;

use App\Helpers\Contracts\RiotApiContract;
use App\Match;
use App\Item;
use App\Player;
use App\Champion;
use GuzzleHttp;

class RiotApi implements RiotApiContract {

    protected $client;
    protected $config = [
        'api_key' => '4f491c30-db3e-43a0-b110-59a4162ae6aa',
        'rate_limit' => 9,
        'region' => 'eune',
        'summoner_v' => 'v1.4',
        'match_v' => 'v2.2',
        'stats_v' => 'v1.3',
        'lol_static_data_v' => 'v1.2',
        'game_v' => '6.11.1'
    ];
    protected $champions;
    protected $seasons = [
        'SEASON3',
        'SEASON2014',
        'SEASON2015',
        'SEASON2016',
    ];
    protected $summoner;

    protected function checkRateLimit($rec) {
        $rate = $rec->getHeader("X-Rate-Limit-Count");
        $rate = explode(',', $rate[0]);
        $rate = explode(':', $rate[0]);

        if ($rate[0] >= $this->config['rate_limit']) {
            sleep(5);
        }
    }

    public function __construct() {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'https://' . $this->config['region'] . '.api.pvp.net/api/lol/' . $this->config['region'] . '/',
            'timeout' => 10.0,
            'verify' => false
        ]);
        $this->champions = $this->getChampionsData();
    }

    /**
     * Gets summoner info from name
     * @param type $name
     * @return object $summonerInfo
     */
    public function getSummonerByName($name) {

        
        $name = mb_strtolower($name,'UTF-8');
        $name = str_replace(' ', '', strtolower($name));
        $summoner = Player::where('name', $name)->first();
        if (!isset($summoner)) {
            $url = $this->config['summoner_v'] . '/summoner/by-name/' . $name . '?api_key=' . $this->config['api_key'];

            try{
                $rec = $this->client->get($url);
            } catch (GuzzleHttp\Exception\RequestException $ex) {
                abort(404, 'Summoner not found');
            }
            
            $this->checkRateLimit($rec);
            $data = json_decode($rec->getBody())->$name;
            $summoner = new Player;
            $summoner->id = $data->id;
            $summoner->name = $name;
            $summoner->data = serialize($data);
            $summoner->save();
            $summoner->data = $data;
        } else {
            $summoner->data = unserialize($summoner->data);
        }
        $this->summoner = $summoner;
        return $summoner;
    }
    
    protected function getSummonerById($id){
        $summoner = Player::find($id);
        
        if (!isset($summoner)) {
            $url = $this->config['summoner_v'] . '/summoner/' . $id . '?api_key=' . $this->config['api_key'];

            

            $rec = $this->client->get($url);
            $this->checkRateLimit($rec);
            $data = json_decode($rec->getBody())->$name;
            $summoner = new Player;
            $summoner->id = $data->id;
            $summoner->name = $name;
            $summoner->data = serialize($data);
            $summoner->save();
            $summoner->data = $data;
        } else {
            $summoner->data = unserialize($summoner->data);
        }
        $this->summoner = $summoner;
        return $summoner;
    }

    /**
     * Gets stats for player by id and season
     * 
     * @param type $id
     * @param type $season
     * @return object $stats
     */
    public function getRankedStats($id, $season = 'SEASON2016') {
        $summoner = Player::find($id);


        if (!isset($summoner)) {
            $summoner = $this->getSummonerById($id);
        }
        if (!is_object(unserialize($summoner->stats))) {
            $url = $this->config['stats_v'] . '/stats/by-summoner/' . $id . '/ranked?season=' . $season . '&api_key=' . $this->config['api_key'];
            try {
                $rec = $this->client->get($url);
            } catch (GuzzleHttp\Exception\RequestException $ex) {
                return abort(404, 'No matches found');
            }
            
            $this->checkRateLimit($rec);
            $stats = json_decode($rec->getBody());
            $summoner->stats = serialize($stats);
            $summoner->save();
        } else {
            $stats = unserialize($summoner->stats);
        }

        return $stats;
    }
    
    

    public function getChampionData($id) {
        if (!isset($this->champions[$id])) {
            $this->champions = $this->getChampionsData();
        }
        return $this->champions[$id];
    }

    public function getChampionsData() {
        $champions = Champion::get();
        $return = '';
        foreach ($champions as $champ) {
            $champ->data = unserialize($champ->data);
            $return[$champ->id] = $champ;
        }
        return $return;
    }

    public function getChampionAvatar($id) {
        $champion = $this->getChampionData($id);
        return 'http://ddragon.leagueoflegends.com/cdn/' . $this->config['game_v'] . '/img/champion/' . $champion->data->image->full;
    }

    /**
     * 
     * @param type $id
     * @param type $begin
     * @param type $end
     * @return object $matchlist
     */
    public function getMatchList($id, $begin = 0, $end = 20, $force = false) {
        $summoner = Player::find($id);

        if (strlen($summoner->matches) < 2 || $force) {
            $url = $this->config['match_v'] . '/matchlist/by-summoner/' . $id . '?beginIndex=' . $begin . '&endIndex=' . $end . '&api_key=' . $this->config['api_key'];
            $rec = $this->client->get($url);
            $this->checkRateLimit($rec);
            $matches = json_decode($rec->getBody())->matches;
            $summoner->matches = serialize($matches);
            $summoner->save();
        } else {
            $matches = unserialize($summoner->matches);
        }
        return $matches;
    }

    /**
     * Gets match data by ID
     * 
     * @param type $id
     * @return type
     */
    public function getMatch($id) {
        $match = Match::find($id);
        if (!$match) {
            $url = $this->config['match_v'] . '/match/' . $id . '?api_key=' . $this->config['api_key'];
            $rec = $this->client->get($url);
            $this->checkRateLimit($rec);
            $data = json_decode($rec->getBody());
            $match = new Match;
            $match->id = $id;
            $match->players = serialize($data->participantIdentities);
            $match->data = serialize($data);
            $match->save();
            $match->players = $data->participantIdentities;
            $match->data = $data;
        } else {
            $match->players = unserialize($match->players);
            $match->data = unserialize($match->data);
        }

        return $match;
    }

}
